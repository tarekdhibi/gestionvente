/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fsg.dao;

import java.sql.PreparedStatement;

import fsg.ent.Achat;
import fsg.tec.Connexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Dell
 */
public class AchatDAO {
    private Achat achat;
    private final Connexion conn;

    public AchatDAO(Achat achat, Connexion conn) {
        this.achat = achat;
        this.conn = conn;
    }
    public AchatDAO() {
        this.achat = new Achat();
        this.conn = new Connexion();
    }
    public void ajouterAchat(int id, int idf,int idp,int q)
{
   PreparedStatement stm;
    try {
          stm = conn.cnx.prepareStatement("INSERT INTO achat VALUES ("+id+", "+idf+", "+idp+", "+q+")");
          stm.execute();
        //ResultSet res = stm.executeQuery();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
}
public void SupprimerAchat (int id)
{
   PreparedStatement stm;
    try {
          stm = conn.cnx.prepareStatement("DELETE FROM achat WHERE id= "+id+"");
          stm.execute();
        //ResultSet res = stm.executeQuery();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
}
public void modifierAchat(int id, int idf,int idp,int q)
{
   PreparedStatement stm;
    try {

          stm = conn.cnx.prepareStatement("UPDATE achat SET idfourn = "+idf+"  , idproduit ="+idp+"  , quantite = "+q+" WHERE id = "+id+";");

        stm.executeUpdate();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
}

public Achat getAchat(int id)
{
    Achat temp = new Achat();
    PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT * FROM achat WHERE id="+id+";");
        ResultSet res = stm.executeQuery();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
    return temp;
}
public int getNbAchat()
{
    int nb = 0;
     PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT COUNT(*) FROM achat;");
        ResultSet nbAdmin = stm.executeQuery();
        while(nbAdmin.next())
        {
        nb+=Integer.parseInt(nbAdmin.getString(1));
        }

    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);

    }
    return nb;
}
public ResultSet getAchats()
{
    PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT * FROM achat;");
        ResultSet allAdmin = stm.executeQuery();
        return allAdmin;
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
    return null;
}


}
