/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fsg.dao;

import fsg.ent.Admin;
import fsg.tec.Connexion;
//import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Dell
 */
public class AdminDAO {
private Admin admin;
private final Connexion conn;
public AdminDAO()
{
    this.admin = new Admin();
    conn = new Connexion();
}
public AdminDAO(Admin a)
{
    this.admin = a;
    conn = new Connexion();
}
public void SetAdmin(Admin a)
{
    this.admin = a;
}
public void ajouterAdmin(int id, String log,String mdp,String role)
{
   PreparedStatement stm;
    try {
          stm = conn.cnx.prepareStatement("INSERT INTO admin VALUES ("+id+", '"+log+"', '"+mdp+"', '"+role+"')");
          stm.execute();
        //ResultSet res = stm.executeQuery();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
}
public void SupprimerAdmin( int id)
{
   PreparedStatement stm;
    try {
          stm = conn.cnx.prepareStatement("DELETE FROM admin WHERE id= "+id+"");
          stm.execute();
        //ResultSet res = stm.executeQuery();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
}
public void modifierAdmin(int idd, String log,String mdp,String role)
{
   PreparedStatement stm;
    try {

          stm = conn.cnx.prepareStatement("UPDATE admin SET log = '"+log+"'  , mdp = '"+mdp+"'  , role = '"+role+"' WHERE id = "+idd+"");

        stm.executeUpdate();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
}
public Admin GetAdmin()
{
    return this.admin;
}
public Admin getAdmin(int id)
{
    Admin temp = new Admin();
    PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT * FROM admin WHERE Id="+id+";");
        ResultSet res = stm.executeQuery();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
    return temp;
}
public int getNbTable(String nomtable)
{
    int nb = 0;
     PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT COUNT(*) FROM "+nomtable);
        ResultSet nbAdmin = stm.executeQuery();
        while(nbAdmin.next())
        {
        nb+=Integer.parseInt(nbAdmin.getString(1));
        }

    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);

    }
    return nb;
}

public ResultSet getAdmins()
{
    PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT * FROM admin");
        ResultSet allAdmin = stm.executeQuery();
        return allAdmin;
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
    return null;
}
public ResultSet getTables(String table)
{
    PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT * FROM "+table);
        ResultSet allAdmin = stm.executeQuery();
        return allAdmin;
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
    return null;
}



public int getNextId()
{
    PreparedStatement stm;
    int idAdmin=-1;
    try {
    stm=conn.cnx.prepareStatement("SELECT ID FROM admin");
    ResultSet nbPersonne = stm.executeQuery();
    while(nbPersonne.next()) {
        if(idAdmin<  nbPersonne.getInt(1))
         idAdmin= nbPersonne.getInt(1);
        }
    }
    catch (Exception e)
    {
JOptionPane.showMessageDialog( null,  e.getMessage()+"Erreur::"+e.getCause(), "Erreur", JOptionPane.ERROR_MESSAGE);
    }
    return idAdmin+1;
}
public int getNextIdTable(String table)
{
    PreparedStatement stm;
    int idAdmin=-1;
    try {
    stm=conn.cnx.prepareStatement("SELECT id FROM "+table);
    ResultSet nbPersonne = stm.executeQuery();
    while(nbPersonne.next()) {
        if(idAdmin< Integer.parseInt( nbPersonne.getString(1)))
         idAdmin= Integer.parseInt( nbPersonne.getString(1));
        }
    }
    catch (Exception e)
    {
JOptionPane.showMessageDialog( null, "Erreur::"+e.getCause(), "Erreur", JOptionPane.ERROR_MESSAGE);
    }
    return idAdmin+1;
}
public int Authentification(String log, String mdp)
{
    PreparedStatement stm;
    int idAdmin=-1;
    try {
    stm=conn.cnx.prepareStatement("SELECT id FROM admin where log='"+log+"' AND mdp='"+mdp+"'");
    ResultSet nbPersonne = stm.executeQuery();
    while(nbPersonne.next()) {
         idAdmin= Integer.parseInt( nbPersonne.getString(1));
        }
    }
    catch (Exception e)
    {
JOptionPane.showMessageDialog( null, "Erreur::"+e.getCause(), "Erreur", JOptionPane.ERROR_MESSAGE);
    }
    return idAdmin;
}
public String GetRole(int id)
{
    String role = "" ;
    PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT Role FROM admin where Id="+id+";");
        ResultSet RoleAdmin = stm.executeQuery();
        while(RoleAdmin.next()) {
        role = RoleAdmin.getString(1);
        }
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
    return role;
}
public static void main (String args[]){
    AdminDAO a = new AdminDAO();
   System.err.println( a.Authentification("a", "a"));
}
}
