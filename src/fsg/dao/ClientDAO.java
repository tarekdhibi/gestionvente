/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fsg.dao;

import java.sql.PreparedStatement;
import fsg.ent.Client;
import fsg.tec.Connexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Dell
 */
public class ClientDAO {
     private Client client;
    private final Connexion conn;

    public ClientDAO(Client client, Connexion conn) {
        this.client = client;
        this.conn = conn;
    }
    public ClientDAO() {
        this.client = new Client();
        this.conn = new Connexion();
    }

    public void ajouterClient(int id, String nom,String prenom,String tel, String adresse)
{
   PreparedStatement stm;
    try {
          stm = conn.cnx.prepareStatement("INSERT INTO client VALUES ("+id+", '"+nom+"', '"+prenom+"', '"+tel+"', '"+adresse+"')");
          stm.execute();
        //ResultSet res = stm.executeQuery();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
}
    public String getNomClient(int id)
{
    String nb = "";
     PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT nom FROM client WHERE id="+id+"");
        ResultSet nbAdmin = stm.executeQuery();
        while(nbAdmin.next())
        {
        nb=nbAdmin.getString(1);
        //System.err.print(nb);
        }

    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);

    }
    return nb;
}
public void SupprimerClient (int id)
{
   PreparedStatement stm;
    try {
          stm = conn.cnx.prepareStatement("DELETE FROM client WHERE id= "+id+"");
          stm.execute();
        //ResultSet res = stm.executeQuery();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
}
public void modifierClient(int idd, String nom,String prenom,String tel,String adresse)
{
   PreparedStatement stm;
    try {

          stm = conn.cnx.prepareStatement("UPDATE client SET nom = '"+nom+"'  , prenom = '"+prenom+"'  , tel = '"+tel+"', adresse = '"+adresse+"' WHERE id = "+idd+"");

        stm.executeUpdate();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
}

public Client getClient(int id)
{
    Client temp = new Client();
    PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT * FROM client WHERE Id="+id+";");
        ResultSet res = stm.executeQuery();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
    return temp;
}
public int getNbClient()
{
    int nb = 0;
     PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT COUNT(*) FROM client;");
        ResultSet nbAdmin = stm.executeQuery();
        while(nbAdmin.next())
        {
        nb+=Integer.parseInt(nbAdmin.getString(1));
        }

    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);

    }
    return nb;
}
public ResultSet getClients()
{
    PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT * FROM client;");
        ResultSet allAdmin = stm.executeQuery();
        return allAdmin;
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
    return null;
}

}
