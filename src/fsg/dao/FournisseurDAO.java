/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fsg.dao;

import java.sql.PreparedStatement;
import fsg.ent.Fourn;
import fsg.tec.Connexion;
//import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Dell
 */
public class FournisseurDAO {
    private Fourn fournisseur;
    private final Connexion conn;

    public FournisseurDAO(Fourn fournisseur, Connexion conn) {
        this.fournisseur = fournisseur;
        this.conn = conn;
    }

    public FournisseurDAO() {
        this.fournisseur = new Fourn();
        conn = new Connexion();
    }

    
    
    public Fourn getfournisseur() {
        return fournisseur;
    }

    public Connexion getConn() {
        return conn;
    }

    public void setfournisseur(Fourn fournisseur) {
        this.fournisseur = fournisseur;
    }
    public void ajouterFournisseur(int id, String nom,String prenom,String tel, String adresse)
{
   PreparedStatement stm;
    try {
          stm = conn.cnx.prepareStatement("INSERT INTO fourn  VALUES ("+id+", '"+nom+"', '"+prenom+"', '"+tel+"', '"+adresse+"')");
          stm.execute();
        //ResultSet res = stm.executeQuery();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
}
public void SupprimerFournisseur( int id)
{
   PreparedStatement stm;
    try {
          stm = conn.cnx.prepareStatement("DELETE FROM fourn WHERE id= "+id+"");
          stm.execute();
        //ResultSet res = stm.executeQuery();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
}
public void modifierFourn(int idd, String nom,String prenom,String tel,String adresse)
{
   PreparedStatement stm;
    try {

          stm = conn.cnx.prepareStatement("UPDATE fourn SET nom = '"+nom+"'  , prenom = '"+prenom+"'  , tel = '"+tel+"', adresse = '"+adresse+"' WHERE id = "+idd+";");

        stm.executeUpdate();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
}

public Fourn getFourn(int id)
{
    Fourn temp = new Fourn();
    PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT * FROM fourn WHERE Id="+id+";");
        ResultSet res = stm.executeQuery();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
    return temp;
}
public int getNbFourn()
{
    PreparedStatement stm;
    int nb = 0;
     
    try {
       stm =( conn.cnx.prepareStatement("SELECT COUNT(*) FROM fourn"));
    //stm=conn.cnx.prepareStatement("SELECT id FROM admin where log='");
        ResultSet nbAdmin = stm.executeQuery();
        while(nbAdmin.next())
        {
        nb+=Integer.parseInt(nbAdmin.getString(1));
        }

    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);

    }
    return nb;
}
public String getNomFourn(int id)
{
    String nb = "";
     PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT nom FROM fourn WHERE id="+id+"");
        ResultSet nbAdmin = stm.executeQuery();
        while(nbAdmin.next())
        {
        nb=nbAdmin.getString(1);
        //System.err.print(nb);
        }

    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);

    }
    return nb;
}
public ResultSet getFourns()
{
    PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT * FROM fourn;");
        ResultSet allAdmin = stm.executeQuery();
        return allAdmin;
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
    return null;
}

}
