/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fsg.dao;

import java.sql.PreparedStatement;
import fsg.ent.Produit;
import fsg.tec.Connexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Dell
 */
public class ProduitDAO {

    private Produit produit;
    private final Connexion conn;

    public ProduitDAO(Produit produit, Connexion cnx) {
        this.produit = produit;
        this.conn = cnx;
    }

    public ProduitDAO() {
        this.produit = new Produit();
        this.conn = new Connexion();
    }

 public void ajouterProduit(int id, String libeller,String prixv ,String prixa, String type, String nbe)
{
   PreparedStatement stm;
    try {
          stm = conn.cnx.prepareStatement("INSERT INTO produit VALUES ("+id+", '"+libeller+"', '"+prixa+"', '"+prixv+"', '"+type+"', '"+nbe+"')");
          stm.execute();
        //ResultSet res = stm.executeQuery();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
}
public void SupprimerProduit (int id)
{
   PreparedStatement stm;
    try {
          stm = conn.cnx.prepareStatement("DELETE FROM produit WHERE id= "+id+"");
          stm.execute();
        //ResultSet res = stm.executeQuery();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
}
public String getNomProduit(int id)
{
    String nb = "";
     PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT libelle FROM produit WHERE id="+id+"");
        ResultSet nbAdmin = stm.executeQuery();
        while(nbAdmin.next())
        {
        nb=nbAdmin.getString(1);
        }

    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);

    }
    return nb;
}
public void modifierProduit(int idd, String libeller,String prixv ,String prixa, String type, String nbe)
{
   PreparedStatement stm;
    try {

          stm = conn.cnx.prepareStatement("UPDATE produit SET libelle = '"+libeller+"'  , prixvente = '"+prixv+"'  , prixachat = '"+prixa+"', type = '"+type+"', nbexemp = '"+nbe+"' WHERE id = "+idd+"");

        stm.executeUpdate();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
}
public void modifierProduit(int idd,  String nbe)
{
   PreparedStatement stm;
    try {

          stm = conn.cnx.prepareStatement("UPDATE produit SET nbexemp = '"+nbe+"' WHERE id = "+idd+"");

        stm.executeUpdate();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
}

public Produit getProduit(int id)
{
    Produit temp = new Produit();
    PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT * FROM produit WHERE Id="+id+";");
        ResultSet res = stm.executeQuery();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
    return temp;
}
public int getNbProduit()
{
    int nb = 0;
     PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT COUNT(*) FROM produit");
        ResultSet nbAdmin = stm.executeQuery();
        while(nbAdmin.next())
        {
        nb+=Integer.parseInt(nbAdmin.getString(1));
        }

    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);

    }
    return nb;
}
public int getNbExProduit(int id)
{
    int nb = 0;
     PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT nbexemp FROM produit WHERE id="+id+"");
        ResultSet nbAdmin = stm.executeQuery();
        while(nbAdmin.next())
        {
        nb+=Integer.parseInt(nbAdmin.getString(1));
        }

    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);

    }
    return nb;
}
public ResultSet getProduits()
{
    PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT * FROM produit;");
        ResultSet allAdmin = stm.executeQuery();
        return allAdmin;
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
    return null;
}

}
