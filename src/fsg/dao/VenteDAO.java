/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fsg.dao;

import java.sql.PreparedStatement;
import fsg.ent.Vente;
import fsg.tec.Connexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Dell
 */
public class VenteDAO {
    private Vente vente;
    private final Connexion conn;
 

    public VenteDAO(Vente vente, Connexion conn) {
        this.vente = vente;
        this.conn = conn;
    }
    public VenteDAO() {
        this.vente = new Vente();
        this.conn = new Connexion();
    }
    public void ajouterVente(int id, int idc,int idp,int q)
{
   PreparedStatement stm;
    try {
          stm = conn.cnx.prepareStatement("INSERT INTO vente  VALUES ("+id+", "+idc+", "+idp+", "+q+")");
          stm.execute();
        //ResultSet res = stm.executeQuery();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
}
public void SupprimerVente (int id)
{
   PreparedStatement stm;
    try {
          stm = conn.cnx.prepareStatement("DELETE FROM vente WHERE id= "+id+"");
          stm.execute();
        //ResultSet res = stm.executeQuery();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
}
public void modifierVente(int id, int idc,int idp,int q)
{
   PreparedStatement stm;
    try {

          stm = conn.cnx.prepareStatement("UPDATE Vente SET idclient = "+idc+"  , idproduit ="+idp+"  , quantite = "+q+" WHERE id = "+id+"");

        stm.executeUpdate();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
}

public Vente getVente(int id)
{
    Vente temp = new Vente();
    PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT * FROM Vente WHERE id="+id+"");
        ResultSet res = stm.executeQuery();
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
    return temp;
}
public int getNbVente()
{
    int nb = 0;
     PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT COUNT(*) FROM vente");
        ResultSet nbAdmin = stm.executeQuery();
        while(nbAdmin.next())
        {
        nb+=Integer.parseInt(nbAdmin.getString(1));
        }

    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);

    }
    return nb;
}
public ResultSet getVentes()
{
    PreparedStatement stm;
    try {
        stm = conn.cnx.prepareStatement("SELECT * FROM vente");
        ResultSet allAdmin = stm.executeQuery();
        return allAdmin;
    } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "description de l'erreur : "+ ex.getMessage(), "Erreur",JOptionPane.ERROR_MESSAGE);
    }
    return null;
}


}
