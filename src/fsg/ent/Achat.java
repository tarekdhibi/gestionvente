/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fsg.ent;

/**
 *
 * @author Dell
 */
public class Achat {
    private int id;
    private int idFourn;
    private int idProduit;
    private int quantite;

    public Achat(int id, int idFourn, int idProduit, int quantite) {
        this.id = id;
        this.idFourn = idFourn;
        this.idProduit = idProduit;
        this.quantite = quantite;
    }
    public Achat() {
            this.id = 0;
            this.idFourn = 0;
            this.idProduit = 0;
            this.quantite = 0;
        }
    public void setId(int id) {
        this.id = id;
    }

    public void setIdFourn(int idFourn) {
        this.idFourn = idFourn;
    }

    public void setIdProduit(int idProduit) {
        this.idProduit = idProduit;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public int getId() {
        return id;
    }

    public int getIdFourn() {
        return idFourn;
    }

    public int getIdProduit() {
        return idProduit;
    }

    public int getQuantite() {
        return quantite;
    }

}
