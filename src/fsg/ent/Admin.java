/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fsg.ent;

/**
 *
 * @author Dell
 */
public class Admin {
    private int id;
    private String log;
    private String mdp;
    private String role;

    public Admin(int id, String log, String mdp, String role) {
        this.id = id;
        this.log = log;
        this.mdp = mdp;
        this.role = role;
    }


    public Admin( String log, String mdp, String role) {
        this.id = 0;
        this.log = log;
        this.mdp = mdp;
        this.role = role;
    }
    public Admin( String log, String mdp) {
        this.id = 0;
        this.log = log;
        this.mdp = mdp;
        this.role = "";
    }
    public Admin() {
        this.id = 0;
        this.log = "";
        this.mdp = "";
        this.role = "";
    }

    public int getId() {
        return id;
    }

    public String getLog() {
        return log;
    }

    public String getMdp() {
        return mdp;
    }

    public String getRole() {
        return role;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public void setRole(String role) {
        this.role = role;
    }
    

}
