/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fsg.ent;

/**
 *
 * @author Dell
 */
public class Client {

    private int id;
    private String nomc;
    private String prenomc;
    private String telc;
    private String adressec;

    public Client(int idc, String nomc, String prenomc, String telc, String adressec) {
        this.id = idc;
        this.nomc = nomc;
        this.prenomc = prenomc;
        this.telc = telc;
        this.adressec = adressec;
    }

  public Client() {
        this.id =0;
        this.nomc= "";
        this.prenomc = "";
        this.telc = "";
        this.adressec= "";

    }
    public String getAdressec() {
        return adressec;
    }

    public int getIdc() {
        return id;
    }

    public String getNomc() {
        return nomc;
    }

    public String getPrenomc() {
        return prenomc;
    }

    public String getTelc() {
        return telc;
    }

    public void setAdressec(String adressec) {
        this.adressec = adressec;
    }

    public void setIdc(int idc) {
        this.id = idc;
    }

    public void setNomc(String nomc) {
        this.nomc = nomc;
    }

    public void setPrenomc(String prenomc) {
        this.prenomc = prenomc;
    }

    public void setTelc(String telc) {
        this.telc = telc;
    }



}
