/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fsg.ent;

/**
 *
 * @author Dell
 */
public class Fourn {


    private int id;
    private String nom;
    private String prenom;
    private String tel;
    private String adresse;

    public Fourn(int id, String nom, String prenom,String tel,String adresse) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.tel = tel;
        this.adresse= adresse;

    }
    public Fourn() {
        this.id = 0;
        this.nom = "";
        this.prenom = "";
        this.tel = "";
        this.adresse= "";

    }


    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getTel() {
        return tel;
    }

      public String getAdresse() {
        return adresse;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
      public void setAdresse(String adresse) {
        this.adresse = adresse;
    }




}
