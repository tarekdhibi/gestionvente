/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fsg.ent;

/**
 *
 * @author Dell
 */
public class Produit {

     private int id;
    private String libelle;
    private String prixvente ;
    private String prixachat;
    private String type ;
    private String nbexemp ;

    public Produit(int id, String libelle, String prixvente, String prixachat, String type, String nbexemp) {
        this.id = id;
        this.libelle = libelle;
        this.prixvente = prixvente;
        this.prixachat = prixachat;
        this.type = type;
        this.nbexemp = nbexemp;
    }

     public Produit() {
         this.libelle = "";
        this.prixvente = "";
        this.prixachat = "";
        this.type = "";
        this.nbexemp = "";

    }

    public int getId() {
        return id;
    }

    public String getLibelle() {
        return libelle;
    }

    public String getNbexemp() {
        return nbexemp;
    }

    public String getPrixachat() {
        return prixachat;
    }

    public String getPrixvente() {
        return prixvente;
    }

    public String getType() {
        return type;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public void setNbexemp(String nbexemp) {
        this.nbexemp = nbexemp;
    }

    public void setPrixachat(String prixachat) {
        this.prixachat = prixachat;
    }

    public void setPrixvente(String prixvente) {
        this.prixvente = prixvente;
    }

    public void setType(String type) {
        this.type = type;
    }

   
}
