/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fsg.ent;

/**
 *
 * @author Dell
 */
public class Vente {

     private int id;
    private int idClent;
    private int idProduit;
    private int quantite;

    public Vente(int id, int idClent, int idProduit, int quantite) {
        this.id = id;
        this.idClent = idClent;
        this.idProduit = idProduit;
        this.quantite = quantite;
    }
    public Vente() {
        this.id = 0;
        this.idClent = 0;
        this.idProduit = 0;
        this.quantite = 0;
    }

    public int getId() {
        return id;
    }

    public int getIdClent() {
        return idClent;
    }

    public int getIdProduit() {
        return idProduit;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIdClent(int idClent) {
        this.idClent = idClent;
    }

    public void setIdProduit(int idProduit) {
        this.idProduit = idProduit;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }


}
